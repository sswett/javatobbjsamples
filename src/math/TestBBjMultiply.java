package math;

import com.basis.bbj.bridge.*;

public class TestBBjMultiply 
{

	final static int TIMEOUT = -1;

	
	public static void main(String[] args) 
	{
		JavaBBjBridge bridge = null;

		try
		{
			String cmdLine = " -cconfig_BBjAppSamples.txt -tT0 -d - a b c ";
			bridge = JavaBBjBridgeFactory.createBridge(cmdLine, true, true, false, true);

			BBjBridgeRequest request = JavaBBjBridgeFactory.createBridgeRequest();
			BBjBridgeResponse response = JavaBBjBridgeFactory.createBridgeResponse();

			request.setProgramName("/Users/steve/workspace/BBjAppSamples/src/MultiplyOverBridge.bbj");
			request.setProgramParameters("2 3 4");

			bridge.runBBj(request,response,TIMEOUT);

			System.out.printf("The result of 2 * 3 * 4 = %s",  new String(response.getBytes()));
			request.clearData();
		}
		catch(BBjBridgeException e)
		{
			e.printStackTrace();
		}
		finally 
		{
			if (bridge != null)
			{
				bridge.close();
			}
		}
	}
	
	
}
